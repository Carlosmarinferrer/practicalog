const express = require('express');
const app = express();
const port = process.env.PORT || 3000;


const userController = require('./controllers/UserController');
app.use(express.json());
app.listen(port);
console.log("API escuchado en el puerto b" + port);
app.get(
  "/apitechu/v1/hello",
  function(req,res){
    console.log("GET /apitechu/v1/hello");
    res.send({ "msg":"hola desde api"});
}
)

app.post("/apitechu/v1/monstruo/:p1/:p2",
function(req,res) {
  console.log("parametros");
  console.log(req.params);
  console.log("query string");
  console.log(req.query);
  console.log("headers");
  console.log(req.headers);
  console.log("body");
  console.log(req.body);
}
)
//app.get("/apitechu/v1/users",
//function(req, res) {
//  console.log("GET /apitechu/v1/users");
//  var users = require('./usuarios.json');
//  res.sendFile("usuarios.json", {root:__dirname});
//    res.send(users);
//}

//)


app.get("/apitechu/v1/users/", userController.getUsersV1);

app.post("/apitechu/v1/users", userController.createUserV1);



app.delete("/apitechu/v1/users/:id",userController.deleteUserV1);
